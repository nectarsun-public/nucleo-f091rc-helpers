#ifndef _NS_WATCHDOG_TIMER_H
#define _NS_WATCHDOG_TIMER_H

void watchdog_timer_init(void);
void watchdog_timer_update(void);

#endif
